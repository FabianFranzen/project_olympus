﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeJump : MonoBehaviour
{
    public Rigidbody m_Rigidbody;
    public Transform m_Transform;
    public Animator m_Anim;


    public float m_JumpForce = 200f;
    public Vector3 m_Velocity;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Hello World");
    }

    // Update is called once per frame
    void Update()
    {
        m_Velocity = m_Rigidbody.velocity;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Space");
            Jump();
        }
        else
        {
            Debug.Log("Space was not pressed this frame");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(m_Rigidbody.velocity.y);
        Debug.Log(m_Velocity.y);
        if (m_Velocity.y < -4.5f) 
        {

            m_Anim.SetBool("Squash", true);
        }
    }

    private void Jump()
    {
        m_Rigidbody.AddForce(0, m_JumpForce, 0);
        m_Anim.SetBool("Jump", true);
    }


}
