﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Komponent som hanterar alla beteenden som hör till kanonen
/// </summary>
public class Cannon : MonoBehaviour
{
    [Space(5)] // <- Ett trevligt attr ibut som ger lite space i unity inspectorn

    [Header("Värden som kontollerar hur kanonkulan beter sig")]

    /// <summary>
    /// Det här är mängden kraft vi applicerar på kanonkulan när den lämnar pipan.
    /// </summary>
    [Range(100, 1000)] // <- Det här är ett attribut som vi applicerar för att få en fin slider i vår unity inspector
    public float m_CannonForce = 1000f;

    /// <summary>
    /// Det här är mängden variation på kanonkulas riktning när den lämnar pipan
    /// </summary>
    [Range(0, 500)] // <- Det här är ett attribut som vi applicerar för att få en fin slider i vår unity inspector
    public float m_Randomness = 100f;

    [Space(5)]

    [Header("Värden som kontollerar hur kanonen beter sig")]
    
    /// <summary>
    /// Det här värdet styr hur snabbt kanonen roterar
    /// </summary>
    [Range(10,100)] // <- Det här är ett attribut som vi applicerar för att få en fin slider i vår unity inspector
    public float m_RotationSpeed = 50;
    
    /// <summary>
    /// Det här är så länge vi behöver vänta mellan varje avfyrat skott
    /// </summary>
    [Range(0,1)] // <- Det här är ett attribut som vi applicerar för att få en fin slider i vår  
    public float m_CooldownTime = 1f;

    [Space(5)]

    [Header("Referenser till olika objekt som behövs för kanonen")]

    /// <summary>
    /// Det här är punkten som kanonkulan lämnar kanonen ifrån
    /// </summary>
    public Transform m_PipeEnd;
    
    /// <summary>
    /// Det här är kanonkulan som vi satte ihop av en 
    /// sfär och ett script som kontrollerar dess livscykel.
    /// Det är en prefab som ligger i vår project mapp
    /// </summary>
    public GameObject cannonBallPrefab;

    /// <summary>
    /// Det här är en flagga som håller koll på om vår kanon kan avfyra ett skott eller behöver "Cool down" 
    /// Den är markerad "private" för att vi inte behöver använda oss av den utanför den här klassen.
    /// </summary>
    private bool m_IsCoolingDown = false;

    /// <summary>
    /// Den här metoden genomförs varje frame. (90fps den här koden utförs då 90 ggr per sekund)
    /// </summary>
    //void Update()
    //{
    //    // Har spacetangenten tryckts ner sedan senaste bildrutan?
    //    if(Input.GetKeyDown(KeyCode.Space))
    //    {
    //        /// Är kanonen i "cooldown"
    //        /// Om inte... Avfyra ett skott
    //        if(!m_IsCoolingDown)
    //        {
    //            Shoot();
    //        }
    //    }

    //    /// Spara värdet av de intressanta knapparna
    //    bool ArrowDownPressed = Input.GetKey(KeyCode.DownArrow);
    //    bool ArrowUpPressed = Input.GetKey(KeyCode.UpArrow);

    //    // Om bara nedåtpilen är nedtryckt så skall kanonen roteras nedåt
    //    if(ArrowDownPressed && !ArrowUpPressed)
    //    {
    //        RotateCannon(1);
    //    }
    //    // Om bara uppåtpilen är nedtryckt så skall kanonen roteras uppåt
    //    else if(ArrowUpPressed && !ArrowDownPressed)
    //    {
    //        RotateCannon(-1);
    //    }
    //}


    /// <summary>
    /// Den här metoden sköter rotation av kanonen. 
    /// </summary>
    /// <param name="direction">Den tar ett värde som input i form av et heltal (int = integer), 
    /// den representerar riktingen som kanonen skall roteras i</param>
    private void RotateCannon(int direction)
    {
    
        transform.Rotate(direction * m_RotationSpeed * Time.deltaTime, 0, 0);
    }

    /// <summary>
    /// Metod för att skjuta iväg en projektil
    /// </summary>
    public void Shoot()
    {
        /// Skapa en kopia av vår kanonkula på positionen av änden på pipan
        GameObject go = Instantiate(cannonBallPrefab, m_PipeEnd.position, m_PipeEnd.rotation);
        /// Skapa en referens till kanonkulans rigidbody
        Rigidbody rb = go.GetComponent<Rigidbody>();

        /// Applicera kraft på kanonkulan i riktingen pipan pekar
        rb.AddForce(m_PipeEnd.forward * m_CannonForce);
        /// Applicera lite slumpmässig kraft med i det spannet vi själva satte 
        rb.AddForce(Random.Range(-m_Randomness, m_Randomness), Random.Range(-m_Randomness, m_Randomness), Random.Range(-m_Randomness, m_Randomness));

        /// Kolla först att vår cooldown time är över 0
        if (m_CooldownTime > 0)
        {
            /// Flagga att kanonen behöver cooldown
            m_IsCoolingDown = true;
            /// Börja räkna ner cooldown
            StartCoroutine(Countdown(m_CooldownTime));
        }
    }

    /// <summary>
    /// En så kallad "Coroutine" Som kan utföra sin kod i intervaller.
    /// I detta fallet kan vi be den att vänta en satt tid
    /// </summary>
    /// <returns></returns>
    private IEnumerator Countdown(float time)
    {
        /// Vänta den tiden vi skickar med in i funktionen
        yield return new WaitForSeconds(time);
        /// Sätt sedan vår cooldown variabel till falskt så att kanonen kan avfyra skott igen
        m_IsCoolingDown = false;
    }
}